package com.stone.mybatis.mapper;

import com.stone.mybatis.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zou.shiyong
 * @Description
 * @date 2018/11/6 9:30
 * @Email zou.shiyong@frontsurf.com
 */
@Mapper
public interface UserMapper {
    //@Insert(value = "insert into user (name,create_time) values (#{name,jdbcType=VARCHAR},#{createTime,jdbcType=TIMESTAMP})")
    int insert(User record);
    //@Select(value = "select id, name, create_time from user where id = #{id,jdbcType=INTEGER}")
    //@Results(value = { @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP) })
    User selectByPrimaryKey(Integer id);
}
